echo "Enter the full path to the docbookx.dtd schema."
echo "-----------------------------------------------------------------"
echo "To give you two examples from opposite ends of the spectrum..."
echo "on Slack 13.1 this is /usr/share/xml/docbook/xml-dtd-4.5/docbookx.dtd"
echo "on freeBSD this is /usr/local/share/xml/docbook/4.4/docbookx.dtd"
echo "on Kubuntu its /usr/share/xml/docbook/schema/dtd/4.4/docbookx.dtd"
read DOCLOC
export DOCLOC
echo '<!DOCTYPE book PUBLIC "-//OASIS//DTD Docbook XML V4.4//EN"' > docbook.header
echo '"'$DOCLOC'">' >> docbook.header
echo "docbook.header has been created successfully."
echo "Now type  make html or make pdf" 
