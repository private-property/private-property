# Makefile

html: docbook.header *.xml credits
	cat docbook.header *.xml credits > tmp.xml
	xmlto html tmp.xml -o ./html

pdf: docbook.header *.xml credits
	cat docbook.header *.xml credits > tmp.xml
	xmlto fo tmp.xml -o pdf
	fop pdf/tmp.fo pdf/privateProperty_sethKenlon.pdf

tidy:
	-rm -f ./pdf/*.fo tmp.xml

clean:
	-rm -f tmp.xml
	-rm -f *.header
	-rm -f html/*.html
	-rm -f ./pdf/*.pdf
