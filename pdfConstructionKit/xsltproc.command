xsltproc --output myfile.fo --stringparam page.width 4.25in --stringparam page.height 6.875in --stringparam body.font.family Times Roman --stringparam body.font.master 8 --stringparam page.margin.inner .4in --stringparam page.margin.outer .4in --stringparam page.margin.top .2in --stringparam page.margin.bottom .2in --stringparam title.margin.left 0.25in --stringparam body.font.size 8 --stringparam chapter.autolabel 0 ./mystyle.xsl tmp.xml
 
fop myfile.fo myfile.pdf && okular myfile.pdf
